<?php
    // Hooks : action / image ===============================
    function TriviumWordPressi_supports() {
        add_theme_support('post-thumbnails');
    }
    //
    add_action('after_setup_theme','TriviumWordPressi_supports');
    

    // post() par ordre ASC  ===================================
    function wpb_custom_query( $query ) {
    // Make sure we only modify the main query on the homepage
        if( $query->is_main_query() && ! is_admin() && $query->is_home() ) {
            // Set parameters to modify the query
            $query->set( 'orderby', 'title' );
            $query->set( 'order', 'ASC' );
        }
    }
    // HOOK our custom query function to the pre_get_posts 
    add_action( 'pre_get_posts', 'wpb_custom_query' );


    // ACF Field: ACF / Tools / Generate / Copy Paste ==========
    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array(
            'key' => 'group_62299069d3599',
            'title' => 'Cat_Animal',
            'fields' => array(
                array(
                    'key' => 'field_62299085bb6ba',
                    'label' => 'Taille',
                    'name' => 'taille',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_6229909bbb6bb',
                    'label' => 'Poids',
                    'name' => 'poids',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_622990b9bb6bc',
                    'label' => 'Mange1',
                    'name' => 'mange1',
                    'type' => 'relationship',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'post_type' => array(
                        0 => 'post',
                    ),
                    'taxonomy' => '',
                    'filters' => array(
                        0 => 'search',
                        1 => 'post_type',
                        2 => 'taxonomy',
                    ),
                    'elements' => '',
                    'min' => '',
                    'max' => '',
                    'return_format' => 'object',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'post',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            'show_in_rest' => 0,
        ));
        
        endif;		

?>