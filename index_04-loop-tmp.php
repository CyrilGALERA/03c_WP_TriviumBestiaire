<?php get_header() ?>
<main>

    <?php if(have_posts()): ?>
    <div class="ligne">

        <?php while(have_posts()): the_post(); ?>
            <div class="box-blue margin-H10">
                
                <?php $lettre1 = get_the_title()[0]; ?>
                <h2> <?= $lettre1; ?> </h2>
                <a href=" <?php the_permalink() ?> "> <?php  the_title(); ?></a>

                <?php while(have_posts()): the_post(); ?>
                    <?php 
                    $lettre2 = get_the_title()[0];
                    if( $lettre1 === $lettre2 ) {?>
                        <a href=" <?php the_permalink() ?> "> <?php  the_title(); ?></a>
                    <?php } else { break; } ?>
                <?php endwhile ?>
            </div>
        <?php endwhile ?>

    <?php else: ?>
        <h2>Pas d'animaux dans le bestiaire... </h2>
    <?php endif ?>

</main>
<?php get_footer() ?>

        



