<?php get_header() ?>
<main>

        <div class="ligne ">
            <!-- <p> img src ou "cmd wp thumbnail... </p> -->
            <?php the_post_thumbnail('medium',['class'=>'box-grey', 'alt' => 'imgArticle', 'style' => 'height: auto;' ]) ?>
            <!-- <img src="..." class="imgMedium" alt="...">  -->

            <div class="box-blue margin-H10 ligne">
                <div>
                    <h2> <?php the_title() ?> </h2>

                    <p> Taille: <?php the_field( 'taille' ); ?> </p>
                    <p> Poid: <?php the_field( 'poids' ); ?> </p>
                    <p> Description: <?php the_content() ?> </p>
                </div>
            </div>
            <div class="box-blue margin-H10 col">
                <div>
                    <h3> Régime Alimentaire: </h3>
                </div>

                <?php 
                $locations = get_field('mange1');
                ?>
                <?php if( $locations ): ?>
                    <ul>
                    <?php foreach( $locations as $location ): ?>
                        <li>
                            <a href="<?php echo get_permalink( $location->ID ); ?>">
                                <?php echo get_the_title( $location->ID ); ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

            </div>
        </div>


</main>
<?php get_footer() ?>

