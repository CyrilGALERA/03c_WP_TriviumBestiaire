<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="16x16" href="/PHP_2022/03c_WP_TriviumBestiaire/wp-content/themes/TriviumBestiaire/screenshot.png">
    <link rel="stylesheet" href="/PHP_2022/03c_WP_TriviumBestiaire/wp-content/themes/TriviumBestiaire/style.css">
    <title>Trivium Bestiaire</title>

    <!-- Barre WordPress juste avant la fin du /head: -->
    <?php wp_head() ?>

</head>

<body id="Top">
    <header>
        <div class="ligne axe1-sp-around axe2-center">
            <a id="id-accueil" class="margin-H50" href=" <?= home_url() ?>"> -Index- </a>
            <!-- <a id="id-accueil" class="margin-H50" href="/PHP_2022/03c_WP_TriviumBestiaire/"> -Index- </a> -->
            <h1 class="dx40 box-grey center-elt center-txt">Le Bestiaire</h1>
        </div>
    </header>




