<?php get_header() ?>
<main>

    <?php if(have_posts()): ?>
    <div class="ligne">

        <?php while(have_posts()): the_post(); ?>
            <div class="box-blue margin-H10">  
                <a href=" <?php the_permalink() ?> "> <h2> <?php the_title() ?> </h2> </a>
            </div>
        <?php endwhile ?>

    </div>
    <?php else: ?>
        <h2>Pas d'animaux dans le bestiaire... </h2>
    <?php endif ?>

</main>
<?php get_footer() ?>