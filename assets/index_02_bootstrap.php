<?php get_header() ?>

<?php if(have_posts()): ?>
    <div class="row">
        <?php while(have_posts()): the_post(); ?>
            <div class="col-sm-4">
                <div class="card" style="width: 18rem;">

                <?php the_post_thumbnail('medium',['class' => 'card-img-top', 'alt' => 'imgArticle', 'style' => 'height: auto;' ]) ?>
                 <!-- the_post_thumbnail('post-thumbnail',['class' => 'card-img-top', 'alt' => 'imgArticle', 'style' => 'height: auto;' ]) ?> -->
                <!-- <img src="..." class="card-img-top" alt="..."> OU:  the_post_thumbnail -->

                    <div class="card-body">
                        <h5 class="card-title"> <?php the_title() ?> </h5>
                        <h6 class="card-subtitle mb-2 text-muted"> Catégorie: <?php the_category() ?> </h6>
                        <h6 class="card-subtitle mb-2 text-muted"> Auteur: <?php the_author() ?> </h6>
                        <!-- <p class="card-text"> <?php the_content() ?> </p> Contenu entier !! -->
                        <p class="card-text"> <?php the_excerpt() ?> </p>
                        <a href=" <?php the_permalink() ?> " class="card-link"> Voir plus</a>
                        <a href="#" class="card-link">Another link</a>
                    </div>
                </div>
            </div>
        <?php endwhile ?>
    </div>
<?php else: ?>
    <h1>Pas d'articles </h1>
<?php endif ?>

<?php get_footer() ?>